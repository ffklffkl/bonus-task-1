import { Server, Response } from 'miragejs';
import { newAccessToken, newRefreshToken, oldRefreshToken } from './constants';

new Server({
  routes() {
    this.get('/users', (_, { requestHeaders }) => {
      const { authorization } = Object.entries(requestHeaders)
        .map(([key, value]) => [key.toLowerCase(), value])
        .reduce((headers, [key, value]) => ({ ...headers, [key]: value }), {});
        
      console.log(authorization);
      return authorization === `Bearer ${newAccessToken}`
        ? new Response(200, undefined, [{ id: 1, name: 'John' }])
        : new Response(401);
    });

    this.get('/auth/refresh', (_, { queryParams: { refreshToken } }) => {
      return refreshToken === oldRefreshToken
        ? new Response(200, undefined, {
            newAccessToken,
            newRefreshToken,
          })
        : new Response(401);
    });
  },
});
