import './server';
import * as axiosApi from './axios';
import * as fetchApi from './fetch';

(async () => {
  await axiosApi.getUsers();
  await fetchApi.getUsers();
})();
