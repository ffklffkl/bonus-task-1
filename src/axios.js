import axios from 'axios';
import { oldAccessToken, oldRefreshToken } from './constants';

const api = axios.create({
  headers: { Authorization: `Bearer ${oldAccessToken}` },
});

function refreshToken() {
  return api
    .get(`auth/refresh?refreshToken=${oldRefreshToken}`, { retry: true })
    .then(({ data }) => data);
}

api.interceptors.response.use(undefined, onRejectRequest);

async function onRejectRequest(err) {
  const { config, response } = err;
  if (response.status === 401 && !config.retry) {
    const { newAccessToken } = await refreshToken();
    config.headers.Authorization = `Bearer ${newAccessToken}`;
    api.defaults.headers.Authorization = `Bearer ${newAccessToken}`;
    return api(config);
  }
  return Promise.reject(err);
}

export function getUsers() {
  return api.get('/users').then((res) => res);
}