export const oldAccessToken = 'old_access_token';
export const oldRefreshToken = 'old_refresh_token';
export const newAccessToken = 'new_access_token';
export const newRefreshToken = 'new_refresh_token';
