import { oldAccessToken, oldRefreshToken } from './constants';

const config = {
  headers: {
    Authorization: `Bearer ${oldAccessToken}`,
  },
};

const api = {
  get(url, retry = false) {
    return fetch(url, {
      method: 'GET',
      headers: config.headers,
    }).then((res) => {
      const { status, statusText } = res;
      if (status >= 400) {
        return Promise.reject({ retry, status, statusText });
      }
      return res.json();
    });
  },
};

function refreshToken() {
  return api.get(`auth/refresh?refreshToken=${oldRefreshToken}`, true);
}

export function getUsers() {
  return api.get('/users').catch(async (err) => {
    if (err.status === 401 && !err.retry) {
      const { newAccessToken } = await refreshToken();
      config.headers.Authorization = `Bearer ${newAccessToken}`;
      return getUsers();
    }
    return Promise.reject(err);
  });
}
